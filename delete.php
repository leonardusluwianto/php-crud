<?php 

    include('./config/db_connect.php');

    function delete($nip) {
        global $conn;

        $sql = "DELETE FROM karyawan WHERE nip = $nip";

        mysqli_query($conn, $sql);

        return mysqli_affected_rows($conn);
    }

    $nip = $_GET['nip'];

    if (delete($nip) > 0) {
        echo "
            <script>
                alert('DATA BERHASIL DIHAPUS');
                document.location.href = 'index.php';
            </script>
        ";
    } else {
        echo "
            <script>
                alert('DATA GAGAL DIHAPUS');
                document.location.href = 'index.php';
            </script>
        ";
    }

?>