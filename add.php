<?php

    include('./config/db_connect.php');

    // Query the salary grade data
    $sql = "SELECT * FROM salary_grade";

    // Make query & get result
    $salary_categories = mysqli_query($conn, $sql);

    // Array of error types
    $errors = array('isEmpty'=>'', 'invalidDate'=>'');

    function query($query) {
        global $conn;
        $result = mysqli_query($conn, $query);
        $rows = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row; 
        }
        return $rows;
    }

    if (isset($_POST['submit'])) { 
        // echo $_POST['name'];
        // echo $_POST['entrydate'];
        // echo $_POST['grade'];
        // getSalary($_POST['grade']);

        // // Check name
        // if (empty($_POST['name'])) {
        //     $errors['name'] = 'Nama tidak boleh kosong <br/>';
        // } else {
        //     $name = $_POST['name'];
        // }

        // // Check gender
        // if (empty($_POST['gender'])) {
        //     $errors['gender'] = 'Gender tidak boleh kosong <br/>';
        // } else {
        //     $gender = $_POST['gender'];
        // }

        // // Check birthdate
        // if (empty($_POST['birthdate'])) {
        //     $errors['birthdate'] = 'Tanggal Lahir tidak boleh kosong <br/>';
        // } else {
        //     $birthdate = $_POST['birthdate'];
        // }

        // // Check entrydate
        // if (empty($_POST['entrydate'])) {
        //     $errors['entrydate'] = 'Tanggal Masuk tidak boleh kosong <br/>';
        // } else {
        //     $entrydate = $_POST['entrydate'];
        // }

        // // Check grade
        // if (empty($_POST['grade'])) {
        //     $errors['grade'] = 'Grade tidak boleh kosong <br/>';
        // } else {
        //     $grade = $_POST['grade'];
        // }
        
        // Check if all fields are filled
        if (empty($_POST['name']) || empty($_POST['gender']) || empty($_POST['birthdate']) || empty($_POST['entrydate']) || empty($_POST['grade'])) {

            $errors['isEmpty'] = 'DATA BELUM LENGKAP';

        } else {
            $name = $_POST['name'];
            $gender = $_POST['gender'];
            $grade = $_POST['grade'];
            
            // Check if entryDate not less than birthDate
            if ($_POST['entrydate'] < $_POST['birthdate']) {
                $errors['invalidDate'] = 'PROSES INPUT SALAH';
            } else {
                $birthdate = $_POST['birthdate'];
                $entrydate = $_POST['entrydate'];
            }
        }

        // Redirect to homepage if no errors
        if (array_filter($errors)) {
            // echo 'Form is invalid.';
        } else {
            $option = query("SELECT * FROM salary_grade");

            $name = mysqli_real_escape_string($conn, $_POST['name']);
            $gender = mysqli_real_escape_string($conn, $_POST['gender']);
            $birthdate = mysqli_real_escape_string($conn, $_POST['birthdate']);
            $entrydate = mysqli_real_escape_string($conn, $_POST['entrydate']);
            $grade = mysqli_real_escape_string($conn, $_POST['grade']);

            foreach($option as $rows) {
                if ($grade == $rows['grade']) {
                    $salary = $rows['salary'];
                }
            }

            // Create SQL
            $sql = "INSERT INTO karyawan(name, gender, birthdate, entrydate, grade, salary) VALUES ('$name', '$gender', STR_TO_DATE('$birthdate', '%Y-%m-%d'), STR_TO_DATE('$entrydate', '%Y-%m-%d'), '$grade', '$salary')";

            // Save to DB and check
            if (mysqli_query($conn, $sql)) {
                // success
                header('Location: index.php');
            } else {
                // error
                echo 'query error: ' . mysqli_error($conn);
            }

            
        }
    }
?>

<!DOCTYPE html>
<html>

    <?php include('templates/header.php'); ?>

    <section class="container grey-text">
        <h4 class="center">Masukkan Data Karyawan Baru</h4>
        <form class="white" action="add.php" method="POST">

            <!-- Rendering errors alert -->
            <?php  
                if (!empty($errors['isEmpty'])) {
                    echo '<div class="card red lighten-4 z-depth-0"><div class="card-content red-text">' . $errors['isEmpty'] . '</div></div>';
                }

                if (!empty($errors['invalidDate'])) {
                    echo '<div class="card red lighten-4"><div class="card-content red-text">' . $errors['invalidDate'] . '</div></div>';
                }
            ?>

            <label>Nama:</label>
            <input type="text" name="name">

            <div class="input-field">
                <select name="gender">
                    <option value="" disabled selected>Select gender</option>
                    <option value="male">male</option>
                    <option value="female">female</option>
                </select>
                <label>Gender:</label>
            </div>

            <label>Tanggal Lahir:</label>
            <input type="text" name="birthdate" class="datepicker">

            <label>Tanggal Masuk:</label>
            <input type="text" name="entrydate" class="datepicker">

            <div class="input-field">
                <select name="grade">
                    <option value="" disabled selected>Select grade</option>
                    <?php 
                        while ($category = mysqli_fetch_array($salary_categories, MYSQLI_ASSOC)) {
                    ?>
                        <option value="<?= $category["grade"]; ?>"><?php echo $category["grade"]; ?></option>
                    <?php 
                        }
                    ?>
                </select>
                <label>Grade:</label>
            </div>

            <div class="center">
                <input type="submit" name="submit" value="submit" class="btn brand z-depth-0">
            </div>
        </form>
    </section>
    
</body>
    
</html>