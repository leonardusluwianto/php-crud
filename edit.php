<?php 

    include('./config/db_connect.php');

    // Fetch data from URL
    $nip = $_GET['nip'];

    function query($query) {
        global $conn;
        $result = mysqli_query($conn, $query);
        $rows = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row; 
        }
        return $rows;
    }

    // Query the employees data based on the NIP
    $employee = query("SELECT * FROM karyawan WHERE nip = $nip")[0];
    

    // Retrieve the grade select options
    $select = "SELECT DISTINCT grade FROM salary_grade ORDER BY grade";
    $option = mysqli_query($conn, $select);

    // $edit = mysqli_query($conn, "SELECT * FROM karyawan WHERE nip = $nip");
    // $row = mysqli_fetch_array($edit);
    

    function edit($data) {
        global $conn;
        // Array of error types
        $errors = array('isEmpty'=>'', 'invalidDate'=>'');

        $nip = $data['nip'];
        $name = htmlspecialchars($data['name']);
        $gender = htmlspecialchars($data['gender']);
        $birthdate = htmlspecialchars($data['birthdate']);
        $entrydate = htmlspecialchars($data['entrydate']);
        $grade = htmlspecialchars($data['grade']);
        
        $option = query("SELECT * FROM salary_grade");

        foreach($option as $rows) {
            if ($grade == $rows['grade']) {
                $salary = $rows['salary'];
            }
        }
        
        if (empty($name)) {

            $errors['isEmpty'] = 'DATA BELUM LENGKAP';

        } else if ($entrydate < $birthdate) {

            $errors['invalidDate'] = 'PROSES INPUT SALAH';

        }
        
        if (array_filter($errors)) {
            // echo 'Form is invalid.';
        } else {
            $query = "UPDATE karyawan SET 
                        name = '$name',
                        gender = '$gender',
                        birthdate = STR_TO_DATE('$birthdate', '%Y-%m-%d'), 
                        entrydate = STR_TO_DATE('$entrydate', '%Y-%m-%d'),
                        grade = '$grade',
                        salary = '$salary'
                    WHERE nip = $nip
                    ";

            mysqli_query($conn, $query);

            return mysqli_affected_rows($conn);
        }
    }

    // Check if the submit button is pressed
    if (isset($_POST['submit'])) {
        // Check if the data is edited successfully
        if (edit($_POST) > 0) {
            echo "
                <script>
                    alert('DATA BERHASIL DIUBAH');
                    document.location.href = 'index.php';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('DATA GAGAL DIUBAH');
                    document.location.href = 'index.php';
                </script>
            ";
        }

    }

?>

<!DOCTYPE html>
<html>

    <?php include('templates/header.php'); ?>

    <section class="container grey-text">
        <h4 class="center">Ubah Data Karyawan</h4>
        <form class="white" action="" method="POST">
            <input type="hidden" name="nip" value="<?= $employee["nip"] ?>">

            <!-- Rendering errors alert -->
            <?php  
                if (!empty($errors['isEmpty'])) {
                    echo '<div class="card red lighten-4 z-depth-0"><div class="card-content red-text">' . $errors['isEmpty'] . '</div></div>';
                }

                if (!empty($errors['invalidDate'])) {
                    echo '<div class="card red lighten-4"><div class="card-content red-text">' . $errors['invalidDate'] . '</div></div>';
                }
            ?>

            <label>Nama:</label>
            <input type="text" name="name" value="<?= $employee["name"]; ?>">

            <div class="input-field">
                <select name="gender">
                    <?php 
                        if ($employee['gender'] == "male") {
                            echo "<option>".$employee['gender']."</option>";
                            echo "<option value='female'>female</option>";
                        } else if ($employee['gender'] == "female") {
                            echo "<option>".$employee['gender']."</option>";
                            echo "<option value='male'>male</option>";
                        }
                    ?>
                </select>
                <label>Gender:</label>
            </div>

            <label>Tanggal Lahir:</label>
            <input type="text" name="birthdate" class="datepicker" value="<?= $employee["birthdate"]; ?>">

            <label>Tanggal Masuk:</label>
            <input type="text" name="entrydate" class="datepicker" value="<?= $employee["entrydate"]; ?>">

            <div class="input-field">
                <select name="grade">
                    <option disabled selected>Select grade</option>
                    <?php foreach($option as $rows): ?>

                        <option value="<?php echo $rows['grade']; ?>" <?php if ($employee['grade'] == $rows['grade']) { echo 'selected="selected"'; } ?>><?php echo $rows['grade']; ?>
                        </option>

                    <?php endforeach; ?>
                </select>
                <label>Grade:</label>
            </div>

            <div class="center">
                <input type="submit" name="submit" value="submit" class="btn brand z-depth-0">
            </div>
        </form>
    </section>
    
</body>
    
</html>