<?php

    include('./config/db_connect.php');

    // Write query
    $sql = 'SELECT karyawan.nip, karyawan.name, karyawan.gender, karyawan.birthdate, karyawan.entrydate, karyawan.grade, salary_grade.salary FROM karyawan INNER JOIN salary_grade ON karyawan.grade=salary_grade.grade ORDER BY created_at';

    // Make query & get result
    $result = mysqli_query($conn, $sql);

    // Fetch the resulting rows as an array
    $employees = mysqli_fetch_all($result, MYSQLI_ASSOC);

    // Free result from memory
    mysqli_free_result($result);

    // Close connection
    mysqli_close($conn);

    // print_r($employees);
?>

<!DOCTYPE html>
<html>

    <?php include('templates/header.php') ?>
    
    <div class="container">
        <table>
            <thead>
                <tr>
                    <th>NIP</th>
                    <th>NAMA</th>
                    <th>GENDER</th>
                    <th>TANGGAL LAHIR</th>
                    <th>TANGGAL MASUK</th>
                    <th>GRADE</th>
                    <th>GAJI</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($employees as $employee) { ?>
                
                <tr>
                    <td><?php echo htmlspecialchars($employee['nip']) ?></td>
                    <td><?php echo htmlspecialchars($employee['name']) ?></td>
                    <td><?php echo htmlspecialchars($employee['gender']) ?></td>
                    <td><?php echo htmlspecialchars($employee['birthdate']) ?></td>
                    <td><?php echo htmlspecialchars($employee['entrydate']) ?></td>
                    <td><?php echo htmlspecialchars($employee['grade']) ?></td>
                    <td><?php echo htmlspecialchars($employee['salary']) ?></td>
                    <td>
                        
                        <a href="edit.php?nip=<?= $employee['nip']; ?>" class="btn blue lighten-2">edit</a>
                        <a href="delete.php?nip=<?= $employee['nip']; ?>" onclick="return confirm('Apakah Anda yakin ingin menghapus data karyawan ini?');" class="btn red lighten-2">delete</a>
                        
                    </td>
                </tr>

            <?php } ?>
            </tbody>
        </table>
    </div>

</html>